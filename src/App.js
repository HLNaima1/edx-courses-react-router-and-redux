import React from 'react'
import { BrowserRouter as Router, Route, Link, NavLink, Switch, Redirect, Prompt } from 'react-router-dom'
import { createStore } from 'redux'
import { reducer } from './reducer'

var store = createStore(reducer);

// To do something whenever the store state changes
var unsubscribe = store.subscribe(() => {
  console.log(store.getState());
})

// Stop doing whatever function is passed to the store.subscribe above when the state changes
// unsubscribe();

class TaskList extends React.Component{
  constructor(props){
    super(props);

    this.state = { taskName: ""};

    this.handleClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  addTask = (taskName) => {
    return {
      type: "ADD_TASK",
      payload: taskName
    }
  }
  
  handleClick()
  {
    store.dispatch(this.addTask(this.state.taskName));
    this.setState(() => ({ taskName: "" }))
  }

  handleChange(event)
  {
    const task = event.target.value;
    this.setState(() => ({ taskName: task }))
  }

  render(){
    return (
      <div>
        <input type="text" placeholder="task name here ..." value={this.state.taskName} onChange={this.handleChange} />
        <button onClick={this.handleClick}>Add task</button>
        <ul>
          { 
            store.getState().forEach(task => {
              return (<li> {task} </li>);
            })
          }
        </ul>
      </div>
    );
  }
}

class Home extends React.Component {
  render()
  {
    return (
      <div>
        <p>You're home ! Just kidding. A web page can't be home at all :(</p>
        <ul>
          <li>
            <Link to={this.props.match.url + '/empty'}>Empty</Link>
          </li>
          <li>
            <Link to={this.props.match.url + '/lol'}>LoL</Link>
          </li>
        </ul>
        <Route path={this.props.match.url + '/lol'} render={() => <p> LoL :p </p>} />
      </div>
    );
  }
}

class Other extends React.Component {
  render()
  {
    console.log(this.props.match);
    console.log(this.props.location);
    console.log(this.props.history);

    // Nested URLs
    return (
      <div>
        <h1> Other's content</h1>
        <p>Nothing really interesting under {this.props.match.path} I'm afraid :p. Try adding /phone or /email to the url.</p>
        <Route path={this.props.match.url + '/phone'} render={() => <p>0X XX XX XX XX</p>} />
        <Route path={this.props.match.url + '/email'} render={() => <p>blabla.blabla@blabla.blabla</p>} />
        {/* Prompts when the user tries to navigate away from the url it is rendered under, in this case /other */}
        <Prompt message="Are you sure you want to navigate away ? too bad :(" />
      </div>
    );
  }
}

class App extends React.Component {
  render()
  {
    return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/home">Home</Link>
          </li>
          <li>
            {/*Active style not applied when the isActive event is used */}
            <NavLink exact to="/other" activeStyle={{color:'green'}} isActive={(match) => { console.log("Hello ! here is " + (match ? match.url : "... missing the match object here :p.")) }}>Other (isActive)</NavLink>
          </li>
          <li>
            <NavLink exact to="/other" activeStyle={{color:'green'}}>Other</NavLink>
          </li>
          <li>
            <NavLink to="/tasks" activeStyle={{color:'red'}}>Tasks</NavLink>
          </li>
        </ul>
      </div>
      

      <Route path="/" render={({match, location, history}) => {
          console.log(match);
          console.log(location);
          console.log(history);

          return (
            <p>You are under the {location.pathname} subpath of /. Check the url if you don't believe me.</p>
          );
        }
        } />
      {/* First route to match is applied */}
      <Switch>
        <Route path="/home" component={Home} />
        <Redirect from="/home" to="/"></Redirect>
        <Route path="/tasks" component={TaskList} />
        <Route strict path="/other/" render={() => <p> the strict "other/" route is prior to the "other" route</p>} />
        <Route path="/other" component={Other} />
        <Route strict path="/hello/:name" render={(props) => {
          const names = [ "Lola", "Riki", "Alex" ];
          const name = props.match.params.name;

          let ret = "";
          if(names.includes(name)) {
            ret = (<h1>Hello {props.match.params.name} !</h1>);
          } else {
            ret = (<Redirect push to="/home"></Redirect>);
          }
          
          return (
            <div>
              <Prompt when={!names.includes(name)} message="You're not allowed here. Would you like to be redirected to the home page ?" />
              {ret}
            </div> 
          );
          }}/>
        <Route render={ () => <p> 404 not found :( </p>}/> {/* Default route when there is no match */}
      </Switch>

      <Route children={() => <h3>I am the footer :)</h3>} />
      
    </Router>
    );
  }
}

export default App;